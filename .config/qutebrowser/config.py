import os
import shutil
import subprocess

c.content.cookies.accept = "no-unknown-3rdparty"
c.content.pdfjs = True

editor = shutil.which(os.getenv("EDITOR"))

c.editor.command = ["alacritty", "--command", editor, "{}"]
c.fonts.default_family = ["Input Mono"]
c.url.searchengines = {
    "DEFAULT": "https://duckduckgo.com/?q={}",
    "ddg": "https://duckduckgo.com/?q={}",
    "di": "http://www.dict.cc/?s={}",
    "dw": "https://de.wikipedia.org/w/index.php?search={}",
    "r": "https://reddit.com/r/{}",
    "u": "https://reddit.com/u/{}"
}

config.bind("<f11>", "config-cycle tabs.show never always ;; config-cycle statusbar.show never always ;; fullscreen")

gpg_key = "8CDEC06794F43C7D2074A2042453A395976F7EC4"

config.bind("pw", f"spawn --userscript qute-keepassxc --key {gpg_key}")

c.aliases['mpv'] = 'spawn --userscript view_in_mpv'

config.set('content.notifications.enabled', True, 'https://glowing-bear.org')

config.load_autoconfig()
