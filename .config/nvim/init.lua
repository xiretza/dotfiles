-- restore previous cursor position when opening file
-- ref: :help restore-cursor
local function restore_cursor(opts)
  local ignored_fts = {
    gitcommit = true,
    rebase = true,
    help = true,
  }

  local line, col = unpack(vim.api.nvim_buf_get_mark(opts.buf, '"'))
  local ft = vim.api.nvim_buf_get_option(opts.buf, "filetype")

  local win = vim.fn.win_findbuf(opts.buf)
  if #win ~= 1 then
    return
  end
  local win = win[1]

  if not ignored_fts[ft] and line > 1 and line <= vim.api.nvim_buf_line_count(opts.buf) then
    vim.api.nvim_win_set_cursor(win, {line, col})
  end
end

vim.api.nvim_create_autocmd("BufRead", {
  callback=function(src)
    vim.api.nvim_create_autocmd("FileType", {
      once=true,
      buffer=src.buf,
      callback=restore_cursor,
    })
  end,
})

-- enable persistent undo
vim.o.undofile = true

-- enable selection using mouse
vim.o.mouse = 'vn'

-- show line numbers, relative offsets above and below cursor line
vim.o.number = true
vim.o.relativenumber = true

-- default tab width is 4 spaces
vim.o.tabstop = 4
vim.o.shiftwidth = 0

-- enable tag files
vim.o.tags = 'tags;,TAGS;'

-- highlight search matches, can be cleared by CTRL-L
vim.o.hlsearch = true
-- preview search matches while still typing
vim.o.incsearch = true

-- highlight trailing spaces
vim.o.listchars = 'tab:  ,trail:█'
vim.o.list = true

-- soft line breaks, show marker at start of continuation line
vim.o.linebreak = true
vim.o.showbreak = '↵ '

-- use selection clipboard for yank/put/etc operations
vim.o.clipboard = 'unnamed'

-- Mappings.
-- edit another file named similarly to the current one
vim.api.nvim_set_keymap('n', 'go', ':e <C-R>%', {noremap = true})
-- quick identifier search-and-replace
vim.api.nvim_set_keymap('n', '<F6>', ':%s/\\<<C-R><C-W>\\>//gc<Left><Left><Left>', {noremap = true})

-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { noremap=true, silent=true, buffer=ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<space>f', function()
      vim.lsp.buf.format { async = true }
    end, opts)
  end,
})

-- Add additional capabilities supported by nvim-cmp
local capabilities = require('cmp_nvim_lsp').default_capabilities()

local lspconfig = require('lspconfig')

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local servers = {
  'clangd',
  'rust_analyzer',
  'pylsp',
  'ghdl_ls',
}
local rustc_path = os.getenv("HOME") .. "/dev/rust/rustc"
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_init = function(client)
      -- https://www.reddit.com/r/neovim/comments/12gvms4/this_is_why_your_higlights_look_different_in_90/
      client.server_capabilities.semanticTokensProvider = nil
      local path = client.workspace_folders[1].name

      local settings = client.config.settings
      if client.name == "rust_analyzer" and path == rustc_path then
        -- rustc configuration
        -- https://rustc-dev-guide.rust-lang.org/building/suggested.html#configuring-rust-analyzer-for-rustc
        settings["rust-analyzer"] = vim.tbl_deep_extend("force", settings["rust-analyzer"], {
          linkedProjects = {
            "Cargo.toml",
            "src/tools/x/Cargo.toml",
            "src/bootstrap/Cargo.toml",
            "src/tools/rust-analyzer/Cargo.toml",
            "compiler/rustc_codegen_cranelift/Cargo.toml",
            "compiler/rustc_codegen_gcc/Cargo.toml"
          },
          check = {
            invocationLocation = "root",
            invocationStrategy = "once",
            overrideCommand = { "python3", "x.py", "check", "--json-output" },
          },
          rustfmt = {
            overrideCommand = { "./build/host/rustfmt/bin/rustfmt", "--edition=2021" },
          },
          procMacro = {
            server = "./build/host/stage0/libexec/rust-analyzer-proc-macro-srv",
          },
          cargo = {
            buildScripts = {
              enable = true,
              invocationLocation = "root",
              invocationStrategy = "once",
              overrideCommand = { "python3", "x.py", "check", "--json-output" },
            },
            sysrootSrc = "./library",
            extraEnv = {
              RUSTC_BOOTSTRAP = "1",
            },
          },
          rustc = {
            source = "./Cargo.toml",
          },
        })
      end

      client.notify("workspace/didChangeConfiguration", { settings = settings })
      return true
    end,
    capabilities = capabilities,
    settings = {
      ["rust-analyzer"] = {
        check = {
          command = "clippy"
        },
      },
    },
  }
end

-- luasnip setup
local luasnip = require('luasnip')

-- nvim-cmp setup
local cmp = require('cmp')
cmp.setup {
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  mapping = {
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end,
    ['<S-Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end,
  },
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  },
}

-- nvim-treesitter setup
require'nvim-treesitter.configs'.setup {
  -- Automatically install missing parsers when entering buffer
  auto_install = false,

  highlight = {
    -- `false` will disable the whole extension
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    disable = {},

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}


-- vim: set et ts=2:
