function fish_prompt --description 'Write out the prompt'
	set -l last_status $status

	set -l color_cwd
	set -l suffix
	switch "$USER"
		case root toor
			if set -q fish_color_cwd_root
				set color_cwd $fish_color_cwd_root
			else
				set color_cwd $fish_color_cwd
			end
			set suffix '#'
		case '*'
			set color_cwd $fish_color_cwd
			set suffix '$'
	end

	set -l prompt_date '[' (date '+%H:%M:%S') '] '
	set -l prompt_who (set_color $fish_color_user) "$USER" (set_color normal) @ (set_color $fish_color_host) (prompt_hostname) (set_color normal)
	set -l prompt_pwd (set_color $color_cwd) (prompt_pwd) (set_color normal)

	set -l prompt_netns (ip netns identify)
	if test -n $prompt_netns
		set prompt_netns ' ' (set_color $fish_color_netns) "[$prompt_netns]" (set_color normal)
	else
		set prompt_nets
	end

	set -l prompt_status
	if test $last_status -ne 0
		set prompt_status ' ' (set_color $fish_color_status) "[$last_status]" (set_color normal)
	end

	set -l prompt_vcs (__fish_vcs_prompt) (set_color normal)

	echo -s -n $prompt_date $prompt_who ' ' $prompt_pwd $prompt_netns $prompt_vcs $prompt_status $suffix ' '
end
