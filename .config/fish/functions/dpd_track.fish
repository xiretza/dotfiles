function dpd_track
	curl \
		--no-progress-meter \
		-X POST \
		--data-raw "\"$argv[1]\"" \
		'https://www.mydpd.at/jws.php/parcel/search' | jq .data[0].lifecycle
end
