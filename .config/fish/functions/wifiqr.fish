function wifiqr
	set -l ssid $argv[1]
	set -l psk (nmcli --show-secrets --get-values 802-11-wireless-security.psk connection show $ssid)
	echo "WIFI:T:WPA;S:$ssid;P:$psk;;" | qrencode --type UTF8
end
