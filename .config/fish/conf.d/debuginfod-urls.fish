if ! set --query DEBUGINFOD_URLS
	for f in /etc/debuginfod/*.urls
		set --global --export --append DEBUGINFOD_URLS (cat $f)
	end
end
