set -gx GPG_TTY (tty)
set -gx SSH_AUTH_SOCK $XDG_RUNTIME_DIR/ssh-agent.socket
set -gx EDITOR nvim
set -gx VISUAL $EDITOR
set -gu fish_user_paths $HOME/.local/bin /usr/lib/rustup/bin
set -gx RIPGREP_CONFIG_PATH $HOME/.config/ripgreprc
set -gx GIT_EXTERNAL_DIFF difft

set -gx QT_QPA_PLATFORM wayland
set -gx MOZ_ENABLE_WAYLAND 1
set -gx _JAVA_AWT_WM_NONREPARENTING 1

abbr --set-cursor m 'math "%"'

function multicd
    echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
end
abbr --add dotdot --regex '^\.\.+$' --function multicd

# https://github.com/qutebrowser/qutebrowser/discussions/7938#discussioncomment-8672918
set -gx QT_SCALE_FACTOR_ROUNDING_POLICY RoundPreferFloor
