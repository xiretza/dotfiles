# Setup

```sh
SRC="$(pwd)"
DST="$HOME"

sed 's#/$##' symlinks.txt | while read fname; do
	ln -sT "$SRC/$fname" "$DST/$fname"
done
```
